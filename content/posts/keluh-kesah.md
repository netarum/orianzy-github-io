---
title: "Keluh Kesah"
date: 2021-02-13T17:04:05Z
draft: false
---

Siapa yang peduli dengan ketiadaan ini, jika aku sendiri tidak peduli.

Terbiasa sendiri sampai aku lupa pada dunia ini.

Siapa yang mengerti aku saat ini, jika hati telah mati.

Tak peduli lagi dengan siang malam hari